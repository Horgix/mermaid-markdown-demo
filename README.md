Test :

```mermaid
sequenceDiagram

participant User
participant Browser as Browser (Front)
participant Back as Backend (API)
participant Ext as Other Internal system


Note left of User: 1st login

Note over User: ID
Note over User: Password
User ->>Browser: Input ID
User ->>Browser: Input Password
Browser ->>Back: SendID + Password


# Note over Back: Auth check
Back->>Ext: Authenticate user
Ext->>Back: OK
rect rgb(191, 223, 255)
Note over Back: Symmetrical<br>Encryption key
Back->>Back: Encrypt BEL ID
# Note over Back: Fetch User infos
Back->>Ext: Fetch User Infos
Ext->>Back: User's First name, ...
end


Back ->> Browser: Encrypted ID

Back ->> Browser: User First Name
Back ->> Browser: JWT

Note over Browser: Save encrypted<br>BEL ID<br>to localstorage
Note over Browser: Save First Name<br>to localstorage
```